package ba.unsa.etf.rma.rma28st;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class TransactionsProvider extends ContentProvider {
    static final String PROVIDER_NAME = "ba.unsa.etf.rma.rma28st.cp";
    static final String URL = "content://" + PROVIDER_NAME + "/transactions";
    static final Uri CONTENT_URI = Uri.parse(URL);
    static final int TRANSACTIONS = 1;
    static final int TRANSACTION_ID = 2;
    static final UriMatcher uriMatcher;
    private static HashMap<String, String> TRANSACTIONS_PROJECTION_MAP;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "transactions", TRANSACTIONS);
        uriMatcher.addURI(PROVIDER_NAME, "transactions/#", TRANSACTION_ID);
    }

    SQLiteDatabase database;
    DatabaseHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        database = databaseHelper.getWritableDatabase();
        return database != null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(DatabaseHelper.TRANSACTIONS_TABLE);
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                qb.setProjectionMap(TRANSACTIONS_PROJECTION_MAP);
                break;
            case TRANSACTION_ID:
                qb.appendWhere(DatabaseHelper.TRANSACTION_INTERNAL_ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder.isEmpty()) {
            sortOrder = DatabaseHelper.TRANSACTION_TITLE;
        }
        Cursor c = qb.query(database, projection, selection, selectionArgs,
                null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                return "vnd.android.cursor.dir/vnd.rma.transactions";
            case TRANSACTION_ID:
                return "vnd.android.cursor.item/vnd.rma.transactions";
            default:
                throw new IllegalArgumentException("Unsupported uri: " + uri.toString());
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long rowID = database.insert(DatabaseHelper.TRANSACTIONS_TABLE, "", values);
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                count = database.delete(DatabaseHelper.TRANSACTIONS_TABLE, selection, selectionArgs);
                break;
            case TRANSACTION_ID:
                String id = uri.getPathSegments().get(1);
                System.out.println("ID TO DELETE " + id);
                count = database.delete(DatabaseHelper.TRANSACTIONS_TABLE, DatabaseHelper.TRANSACTION_INTERNAL_ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                count = database.update(DatabaseHelper.TRANSACTIONS_TABLE, values, selection,
                        selectionArgs);
                break;
            case TRANSACTION_ID:
                count = database.update(DatabaseHelper.TRANSACTIONS_TABLE, values, DatabaseHelper.TRANSACTION_INTERNAL_ID + " = " +
                        uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
