package ba.unsa.etf.rma.rma28st;

import java.util.ArrayList;

public interface ITransactionListInteractor {
    ArrayList<Transaction> getTransactions();

    Account getAccount();

    void addTransaction(Transaction transaction);

    void deleteTransaction(Transaction transaction);

    void editTransaction(Transaction transaction);

    void editAccount(Account account);
}
