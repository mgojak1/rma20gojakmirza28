package ba.unsa.etf.rma.rma28st;

import android.content.Context;

import java.time.LocalDate;
import java.util.ArrayList;

public class TransactionListPresenter implements ITransactionListPresenter, RestApi.TransactionsApi, ConnectivityBroadcastReceiver.ConnectionUpdate {
    private ITransactionListView view;
    private TransactionListInteractor interactor;
    private RestApi restApi;
    private boolean connected;


    TransactionListPresenter(ITransactionListView view, Context context) {
        this.view = view;
        this.interactor = new TransactionListInteractor(context);
        this.restApi = new RestApi(context, this);
        this.connected = false;
    }


    @Override
    public void setTransactions() {
        restApi.fetchTransactions();
    }

    @Override
    public void sortTransactions(int filterBy, LocalDate localDate, String sort) {
        if (connected)
            restApi.sortTransactions(filterBy, localDate.getYear(), localDate.getMonthValue(), sort);
        else view.setTransactions(interactor.getTransactions());
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void filterTransactions(int filterBy, LocalDate localDate) {
        if (connected)
            restApi.filterTransactions(filterBy, localDate.getYear(), localDate.getMonthValue());
        else view.setTransactions(interactor.getTransactions());
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void setAccount() {
        restApi.fetchAccount();
    }

    @Override
    public Account getAccount() {
        return interactor.getAccount();
    }

    @Override
    public void addTransaction(Transaction transaction) {
        if (connected) restApi.postTransaction(transaction);
        else {
            interactor.addTransaction(transaction);
            view.setTransactions(interactor.getTransactions());
        }
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
        if (connected) restApi.deleteTransaction(transaction);
        else {
            interactor.deleteTransaction(transaction);
            view.setTransactions(interactor.getTransactions());
        }
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void updateAccount() {
        setAccount();
    }

    @Override
    public void editTransaction(Transaction transaction) {
        if(connected) restApi.editTransaction(transaction);
        else {
            interactor.editTransaction(transaction);
            view.setTransactions(interactor.getTransactions());
        }
    }

    @Override
    public void editAccount(Account account) {
        if(connected) restApi.editAccount(account);
        interactor.editAccount(account);
    }

    @Override
    public void onTransactionsFetched(ArrayList<Transaction> transactions) {
        view.setTransactions(transactions);
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void onAccountFetched(Account account) {
        if (connected) view.setAccount(account);
        else view.setAccount(interactor.getAccount());
    }

    @Override
    public void onTransactionAdded() {
        view.updateDate(0);
    }

    @Override
    public void onAccountEdited() {
        restApi.fetchAccount();
    }

    @Override
    public void onTransactionsFiltered(ArrayList<Transaction> transactions) {
        if (connected) view.setTransactions(transactions);
        else view.setTransactions(interactor.getTransactions());
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void setConnection(boolean connected) {
        this.connected = connected;
        if (!connected) {
            view.setTransactions(interactor.getTransactions());
            view.notifyTransactionListDataSetChanged();
        } else {
            for (Transaction transaction : interactor.getTransactions()) {
                if(transaction.getTitle().contains("Offline transaction ")) transaction.setTitle(transaction.getTitle().replace("Offline transaction", ""));
                restApi.postTransaction(transaction);
                interactor.deleteTransaction(transaction);
            }
            setAccount();
            setTransactions();
            view.updateDate(0);
        }
    }
}
