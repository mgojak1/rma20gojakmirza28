package ba.unsa.etf.rma.rma28st;

import java.time.LocalDate;
import java.util.ArrayList;

class TransactionsModel {
    private ArrayList<Transaction> transactions = new ArrayList<Transaction>() {{
        add(new Transaction(LocalDate.now().minusMonths(1), 2000, "Paycheck", ETransactionType.REGULARINCOME, "Freelancing income", 10, LocalDate.now().plusMonths(7)));
        add(new Transaction(LocalDate.now().plusMonths(1), 500, "New PC", ETransactionType.PURCHASE, "Ebay order", 10, LocalDate.now().plusMonths(1)));
        add(new Transaction(LocalDate.now().plusMonths(1), 200, "Rent", ETransactionType.REGULARPAYMENT, "Rent", 10, LocalDate.now().plusMonths(4)));
        add(new Transaction(LocalDate.now().plusMonths(4), 3000, "Sold old car", ETransactionType.INDIVIDUALINCOME, "Car sold", 10, LocalDate.now().plusMonths(4)));
        add(new Transaction(LocalDate.now().plusMonths(4), 3500, "Bought new motorcycle", ETransactionType.INDIVIDUALPAYMENT, "Yeaaaaaa", 10, LocalDate.now().plusMonths(4)));
        add(new Transaction(LocalDate.now().plusMonths(4).plusDays(2), 100, "Bills", ETransactionType.REGULARPAYMENT, "Bills again", 10, LocalDate.now().plusMonths(12)));
        add(new Transaction(LocalDate.now(), 20, "Expensive meal", ETransactionType.INDIVIDUALPAYMENT, "Gotta live", 10, LocalDate.now()));
        add(new Transaction(LocalDate.now().plusMonths(1), 12, "A gift", ETransactionType.PURCHASE, "For a friend", 10, LocalDate.now().plusMonths(1)));
        add(new Transaction(LocalDate.now().plusMonths(2), 250, "Android phone bought", ETransactionType.PURCHASE, "I can't stand emulator", 10, LocalDate.now().plusMonths(4)));
        add(new Transaction(LocalDate.now().plusMonths(3), 300, "Side job", ETransactionType.REGULARINCOME, "...", 10, LocalDate.now().plusYears(1)));
        add(new Transaction(LocalDate.now().minusMonths(2), 500, "Trip", ETransactionType.REGULARPAYMENT, "To Hawaii", 10, LocalDate.now().minusMonths(1)));
        add(new Transaction(LocalDate.now().minusMonths(3), 50, "Car fix", ETransactionType.INDIVIDUALPAYMENT, "Break pads changed", 10, LocalDate.now().minusMonths(3)));
        add(new Transaction(LocalDate.now(), 12, "Gift card recieved", ETransactionType.INDIVIDUALINCOME, "For Amazon", 10, LocalDate.now()));
        add(new Transaction(LocalDate.now(), 100, "Honest work", ETransactionType.INDIVIDUALINCOME, "....", 10, LocalDate.now()));
        add(new Transaction(LocalDate.now(), 2, "Chocolate", ETransactionType.PURCHASE, "White", 10, LocalDate.now()));
        add(new Transaction(LocalDate.now().minusMonths(4), 10, "Expenses", ETransactionType.REGULARPAYMENT, "No more ideas", 10, LocalDate.now().plusMonths(1)));
        add(new Transaction(LocalDate.now().minusMonths(4), 600, "Motorcycle parts", ETransactionType.INDIVIDUALPAYMENT, "Expensive", 10, LocalDate.now().minusMonths(4)));
        add(new Transaction(LocalDate.now().plusMonths(8), 900, "Vacation", ETransactionType.REGULARPAYMENT, "Trip to Sydney", 10, LocalDate.now().plusMonths(10)));
        add(new Transaction(LocalDate.now().plusMonths(5), 75, "Sold something", ETransactionType.INDIVIDUALINCOME, "....", 10, LocalDate.now().plusMonths(5)));
        add(new Transaction(LocalDate.now().minusMonths(1), 330, "Intership", ETransactionType.REGULARINCOME, "Nice side money", 10, LocalDate.now().plusMonths(4)));
    }};
}
