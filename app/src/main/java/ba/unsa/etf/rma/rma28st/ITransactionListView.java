package ba.unsa.etf.rma.rma28st;

import java.time.LocalDate;
import java.util.ArrayList;

public interface ITransactionListView {
    void setTransactions(ArrayList<Transaction> transactions);

    void notifyTransactionListDataSetChanged();

    void sortTransactions(ArrayList<Transaction> transactions, int filterBy, LocalDate localDate, String sort);

    void filterTransactions(ArrayList<Transaction> transactions, int filterBy, LocalDate localDate);

    void updateDate(int monthChange);

    void setAccount(Account account);

    void addTransaction(Transaction transaction);

    void deleteTransaction(int position);

    void setClick();
}
