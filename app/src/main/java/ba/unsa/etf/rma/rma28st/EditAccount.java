package ba.unsa.etf.rma.rma28st;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class EditAccount extends AsyncTask<String, Void, Void> {
    private RestApi.TransactionsApi caller;

    EditAccount(RestApi.TransactionsApi caller) {
        this.caller = caller;
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("budget", strings[1]);
            jsonParam.put("monthLimit", strings[2]);
            jsonParam.put("totalLimit", strings[3]);


            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(jsonParam.toString());

            System.out.println("RESPONSE: " + conn.getResponseMessage());
            os.flush();
            os.close();
            try {
                return null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            return null;
        }
    }

    protected void onPostExecute(Void v) {
        caller.onAccountEdited();
    }
}
