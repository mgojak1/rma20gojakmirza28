package ba.unsa.etf.rma.rma28st;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    static final String TRANSACTIONS_TABLE = "transactions";
    static final String TRANSACTION_INTERNAL_ID = "_id";
    static final String TRANSACTION_ID = "id";
    static final String TRANSACTION_DATE = "date";
    static final String TRANSACTION_END_DATE = "endDate";
    static final String TRANSACTION_TYPE = "type";
    static final String TRANSACTION_AMOUNT = "amount";
    static final String TRANSACTION_TITLE = "title";
    static final String TRANSACTION_INTERVAL = "interval";
    static final String TRANSACTION_DESCRIPTION = "description";
    private static final String DATABASE_NAME = "RMATransactionsDB.db";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_TRANSACTIONS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TRANSACTIONS_TABLE + " (" +
                    TRANSACTION_INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + TRANSACTION_ID + " INTEGER , "
                    + TRANSACTION_TITLE + " TEXT, "
                    + TRANSACTION_DESCRIPTION + " TEXT, "
                    + TRANSACTION_AMOUNT + " REAL, "
                    + TRANSACTION_DATE + " DATE, "
                    + TRANSACTION_END_DATE + " DATE, "
                    + TRANSACTION_TYPE + " TEXT, "
                    + TRANSACTION_INTERVAL + " INTEGER);";

    private static final String DROP_TRANSACTIONS_TABLE = "DROP TABLE IF EXISTS " + TRANSACTIONS_TABLE;

    public DatabaseHelper(Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TRANSACTIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TRANSACTIONS_TABLE);
        onCreate(db);
    }
}
