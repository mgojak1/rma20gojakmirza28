package ba.unsa.etf.rma.rma28st;

public interface IBudgetPresenter {
    Account getAccount();

    void updateAccount(double totalLimit, double monthLimit);
}
