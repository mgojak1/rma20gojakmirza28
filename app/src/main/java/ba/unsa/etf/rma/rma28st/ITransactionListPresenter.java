package ba.unsa.etf.rma.rma28st;

import java.time.LocalDate;

public interface ITransactionListPresenter {
    void setTransactions();

    void sortTransactions(int filterBy, LocalDate localDate, String sort);

    void filterTransactions(int filterBy, LocalDate localDate);

    void setAccount();

    Account getAccount();

    void addTransaction(Transaction transaction);

    void deleteTransaction(Transaction transaction);

    void updateAccount();

    void editTransaction(Transaction transaction);

    void editAccount(Account account);
}
