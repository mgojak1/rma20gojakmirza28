package ba.unsa.etf.rma.rma28st;

public class BudgetPresenter implements IBudgetPresenter {
    IBudgetInteractor budgetInteractor;

    public BudgetPresenter() {
        budgetInteractor = new BudgetInteractor();
    }

    @Override
    public Account getAccount() {
        return budgetInteractor.getAccount();
    }

    @Override
    public void updateAccount(double totalLimit, double monthLimit) {
        budgetInteractor.updateAccount(totalLimit, monthLimit);
    }
}
