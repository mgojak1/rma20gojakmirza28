package ba.unsa.etf.rma.rma28st;

import android.content.IntentFilter;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity implements TransactionListFragment.OnItemClick, TransactionDetailFragment.TransactionManager, TransactionListFragment.AddTransaction, BudgetFragment.UpdateAccount {
    public ViewPager viewPager;
    public FragmentViewPagerAdapter fragmentViewPagerAdapter;
    private boolean twoPaneMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        NestedScrollView details = findViewById(R.id.transaction_detail_layout);
        if (details != null) {
            twoPaneMode = true;
            TransactionDetailFragment detailFragment = (TransactionDetailFragment) fragmentManager.findFragmentById(R.id.transaction_detail_layout);
            if (detailFragment == null) {
                detailFragment = new TransactionDetailFragment();
                fragmentManager.beginTransaction().
                        replace(R.id.transaction_detail_layout, detailFragment)
                        .commit();
            }
        } else {
            twoPaneMode = false;
            viewPager = findViewById(R.id.view_pager);
            fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(fragmentViewPagerAdapter);
            viewPager.setCurrentItem(101);
        }
        Fragment listFragment = fragmentManager.findFragmentById(R.id.transaction_list_layout);
        if (listFragment == null) {
            listFragment = new TransactionListFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.transaction_list_layout, listFragment)
                    .commit();
        } else {
            if (twoPaneMode)
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onItemClicked(Transaction transaction, int position) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("transaction", transaction);
        arguments.putInt("position", position);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail_layout, detailFragment)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.root_layout, detailFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void setClick() {
        if (!twoPaneMode) return;
        Fragment listFragment = (getSupportFragmentManager().findFragmentById(R.id.transaction_list_layout));
        if (listFragment != null) {
            ((TransactionListFragment) listFragment).setClick();
        }
    }


    @Override
    public void addTransaction(Transaction transaction) {
        if (!twoPaneMode) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        Fragment listFragment = (getSupportFragmentManager().findFragmentById(R.id.transaction_list_layout));
        if (listFragment != null) {
            ((TransactionListFragment) listFragment).addTransaction(transaction);
            ((TransactionListFragment) listFragment).notifyTransactionListDataSetChanged();
        }
    }

    @Override
    public void deleteTransaction(int position) {
        if (!twoPaneMode) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        Fragment listFragment = (getSupportFragmentManager().findFragmentById(R.id.transaction_list_layout));
        if (listFragment != null) {
            ((TransactionListFragment) listFragment).deleteTransaction(position);
            ((TransactionListFragment) listFragment).notifyTransactionListDataSetChanged();
        }

    }

    @Override
    public void transactionChanged(Transaction transaction) {
        if (!twoPaneMode) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        Fragment listFragment = (getSupportFragmentManager().findFragmentById(R.id.transaction_list_layout));
        if (listFragment != null) {
            ((TransactionListFragment) listFragment).editTransaction(transaction);
            ((TransactionListFragment) listFragment).notifyTransactionListDataSetChanged();
        }
    }

    @Override
    public void addTransaction(Account account) {
        Bundle arguments = new Bundle();
        System.out.println(account != null);
        arguments.putParcelable("account", account);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail_layout, detailFragment)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.root_layout, detailFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void updateAccount(Account account) {
        Fragment listFragment = (getSupportFragmentManager().findFragmentById(R.id.root_layout));
        ((TransactionListFragment) listFragment).setAccount(account);
    }

}
