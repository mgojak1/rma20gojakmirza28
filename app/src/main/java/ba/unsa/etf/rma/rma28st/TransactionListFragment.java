package ba.unsa.etf.rma.rma28st;

import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class TransactionListFragment extends Fragment implements ITransactionListView {
    private TextView globalAmountView;
    private TextView limitView;
    private ListView transactionListView;
    private Spinner sortSpinner;
    private Spinner filterSpinner;
    private TextView dateView;
    private Account account;
    private TransactionListAdapter transactionListAdapter;
    private int lastViewSelected = -1;
    private ConnectivityBroadcastReceiver receiver;
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    private ITransactionListPresenter transactionListPresenter;
    private LocalDate localDate;
    private OnItemClick onItemClick;
    private AddTransaction addTransaction;
    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Transaction transaction = transactionListAdapter.getTransaction(position);
            onItemClick.onItemClicked(transaction, position);
            if (lastViewSelected != -1) {
                getViewByPosition(lastViewSelected, transactionListView).setBackgroundColor(getContext().getColor(R.color.transparent));
            }
            getViewByPosition(position, transactionListView).setBackgroundColor(getContext().getColor(R.color.light_gray));
            lastViewSelected = position;
        }
    };

    private View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    private ITransactionListPresenter getPresenter() {
        if (transactionListPresenter == null) {
            transactionListPresenter = new TransactionListPresenter(this, getActivity());
        }
        return transactionListPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transaction_list_fragment, container, false);
        globalAmountView = view.findViewById(R.id.global_amount);
        limitView = view.findViewById(R.id.limit);
        transactionListView = view.findViewById(R.id.transaction_list);
        sortSpinner = view.findViewById(R.id.sort);
        filterSpinner = view.findViewById(R.id.filter);
        dateView = view.findViewById(R.id.date);
        Button nextMonthButton = view.findViewById(R.id.button_right);
        Button previousMonthButton = view.findViewById(R.id.button_left);
        Button addTransactionButton = view.findViewById(R.id.add_transaction);

        transactionListAdapter = new TransactionListAdapter(getActivity(), R.layout.transaction_list_item, new ArrayList<Transaction>());
        transactionListView.setAdapter(transactionListAdapter);
        transactionListView.setOnItemClickListener(listItemClickListener);
        onItemClick = (OnItemClick) getActivity();
        transactionListView.setNestedScrollingEnabled(true);
        addTransaction = (AddTransaction) getActivity();
        localDate = LocalDate.now();

        receiver = new ConnectivityBroadcastReceiver((ConnectivityBroadcastReceiver.ConnectionUpdate) getPresenter());

        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getPresenter().sortTransactions(filterSpinner.getSelectedItemPosition(), localDate, sortSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FilterSpinnerAdapter filterSpinnerAdapter = new FilterSpinnerAdapter(getActivity(), R.layout.filter_spinner_item,
                new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.filter_by))));
        filterSpinner.setAdapter(filterSpinnerAdapter);
        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getPresenter().filterTransactions(position, localDate);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        nextMonthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate(1);
            }
        });
        previousMonthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate(-1);
            }
        });
        addTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTransaction.addTransaction(account);
            }
        });

        // Initial activity setup
        updateDate(0);
        getPresenter().sortTransactions(filterSpinner.getSelectedItemPosition(), localDate, sortSpinner.getSelectedItem().toString());
        getPresenter().updateAccount();
        return view;
    }

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
        transactionListAdapter.setTransactions(transactions);
    }

    @Override
    public void notifyTransactionListDataSetChanged() {
        transactionListAdapter.notifyDataSetChanged();
        onItemClick.setClick();
    }

    @Override
    public void sortTransactions(ArrayList<Transaction> transactions, int filterBy, LocalDate localDate, String sort) {
        getPresenter().sortTransactions(filterBy, localDate, sort);
    }

    @Override
    public void filterTransactions(ArrayList<Transaction> transactions, int filterBy, LocalDate localDate) {
        getPresenter().filterTransactions(filterBy, localDate);
    }

    @Override
    public void updateDate(int monthChange) {
        if (monthChange == -1)
            localDate = localDate.minusMonths(1);
        else if (monthChange == 1) localDate = localDate.plusMonths(1);
        String month = localDate.getMonth().toString().substring(0, 1).toUpperCase().concat(localDate.getMonth().toString().substring(1).toLowerCase());
        dateView.setText(String.format(Locale.getDefault(), "%s, %d", month, localDate.getYear()));
        getPresenter().sortTransactions(filterSpinner.getSelectedItemPosition(), localDate, sortSpinner.getSelectedItem().toString());
    }

    @Override
    public void setAccount(final Account account) {
        this.account = account;
        globalAmountView.setText(String.format(getString(R.string.global_amount), account.getBudget()));
        limitView.setText(String.format(getString(R.string.limit), account.getTotalLimit()));
    }

    @Override
    public void addTransaction(Transaction transaction) {
        getPresenter().addTransaction(transaction);
        if (transaction.getType() == ETransactionType.REGULARINCOME || transaction.getType() == ETransactionType.INDIVIDUALINCOME) {
            account.setBudget(account.getBudget() + transaction.getAmount());
        } else {
            account.setBudget(account.getBudget() - transaction.getAmount());
        }
        getPresenter().editAccount(account);
        onItemClick.setClick();
    }

    public void editTransaction(Transaction transaction) {
        getPresenter().editTransaction(transaction);
        if (transaction.getType() == ETransactionType.REGULARINCOME || transaction.getType() == ETransactionType.INDIVIDUALINCOME) {
            account.setBudget(account.getBudget() + transaction.getAmount());
        } else {
            account.setBudget(account.getBudget() - transaction.getAmount());
        }
        getPresenter().editAccount(account);
        onItemClick.setClick();
    }

    @Override
    public void deleteTransaction(int position) {
        if (position == -1) return;
        Transaction transaction = transactionListAdapter.getTransaction(position);
        getPresenter().deleteTransaction(transaction);
        if (transaction.getType() == ETransactionType.REGULARINCOME || transaction.getType() == ETransactionType.INDIVIDUALINCOME) {
            account.setBudget(account.getBudget() + transaction.getAmount());
        } else {
            account.setBudget(account.getBudget() - transaction.getAmount());
        }
        getPresenter().editAccount(account);
        onItemClick.setClick();
    }

    @Override
    public void setClick() {
        if (transactionListAdapter.getCount() == 0) addTransaction.addTransaction(account);
        else {
            transactionListView.performItemClick(transactionListView,
                    0, transactionListAdapter.getItemId(0));
        }
    }

    public interface OnItemClick {
        void onItemClicked(Transaction transaction, int position);

        void setClick();
    }

    public interface AddTransaction {
        void addTransaction(Account account);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(receiver);
        super.onPause();
    }
}
