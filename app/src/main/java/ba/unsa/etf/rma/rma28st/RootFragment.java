package ba.unsa.etf.rma.rma28st;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class RootFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.root_element, container, false);
        getFragmentManager().beginTransaction().replace(R.id.root_layout, new TransactionListFragment()).commit();
        return view;
    }
}
