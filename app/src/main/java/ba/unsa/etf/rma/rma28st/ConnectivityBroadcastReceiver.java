package ba.unsa.etf.rma.rma28st;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

public class ConnectivityBroadcastReceiver extends BroadcastReceiver {
    ConnectionUpdate presenter;

    ConnectivityBroadcastReceiver(ConnectionUpdate presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) presenter.setConnection(false);
        else presenter.setConnection(true);
    }

    public interface ConnectionUpdate {
        void setConnection(boolean connected);
    }
}
