package ba.unsa.etf.rma.rma28st;

import android.content.Context;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

class RestApi {
    private final String ROOT = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private Context context;
    private String TRANSACTIONS;

    private TransactionsApi caller;

    RestApi(Context context, TransactionsApi caller) {
        this.context = context;
        this.caller = caller;
        TRANSACTIONS = ROOT + "/account/" + context.getString(R.string.api_id) + "/transactions";
    }

    void fetchTransactions() {
        new GetTransactions(caller).execute(TRANSACTIONS);
    }

    void filterTransactions(int typeId, int year, int month) {
        String filterType = String.valueOf(typeId);
        String filterYear = String.valueOf(year);
        String filterMonth = "";

        if (month < 10) filterMonth = "0" + month;
        else filterMonth = String.valueOf(month);

        String filter = "";
        if (typeId == 0) filter = "year=" + filterYear + "&" + "month=" + filterMonth;
        else
            filter = "typeId=" + filterType + "&" + "year=" + filterYear + "&" + "month=" + filterMonth;
        new FilterTransactions(caller).execute(TRANSACTIONS, filter);
    }

    void sortTransactions(int typeId, int year, int month, String sort) {
        String filterType = String.valueOf(typeId);
        String filterYear = String.valueOf(year);
        String filterMonth = "";

        if (month < 10) filterMonth = "0" + month;
        else filterMonth = String.valueOf(month);

        if (sort.equals("Price - Ascending")) sort = "amount.asc";
        else if (sort.equals("Price - Descending")) sort = "amount.desc";
        else if (sort.equals("Title - Ascending")) sort = "title.desc";
        else if (sort.equals("Title - Descending")) sort = "title.desc";
        else if (sort.equals("Date - Ascending")) sort = "date.desc";
        else if (sort.equals("Date - Descending")) sort = "date.desc";

        String filter = "";
        if (typeId == 0)
            filter = "year=" + filterYear + "&" + "month=" + filterMonth + "&sort=" + sort;
        else
            filter = "typeId=" + filterType + "&" + "year=" + filterYear + "&" + "month=" + filterMonth + "&sort=" + sort;
        new FilterTransactions(caller).execute(TRANSACTIONS, filter);
    }


    void fetchAccount() {
        new GetAccount(caller).execute(ROOT + "/account/" + context.getString(R.string.api_id));
    }

    void postTransaction(Transaction transaction) {
        int typeId = 0;
        if (transaction.getType() == ETransactionType.REGULARPAYMENT) typeId = 1;
        if (transaction.getType() == ETransactionType.REGULARINCOME) typeId = 2;
        if (transaction.getType() == ETransactionType.PURCHASE) typeId = 3;
        if (transaction.getType() == ETransactionType.INDIVIDUALINCOME) typeId = 4;
        if (transaction.getType() == ETransactionType.INDIVIDUALPAYMENT) typeId = 5;
        new PostTransaction(caller).execute(
                TRANSACTIONS,
                transaction.getTitle(),
                String.valueOf(transaction.getAmount()),
                LocalDateTime.of(transaction.getDate(), LocalTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")),
                LocalDateTime.of(transaction.getEndDate(), LocalTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")),
                transaction.getItemDescription(),
                String.valueOf(transaction.getTransactionInterval()),
                String.valueOf(typeId));
    }

    void deleteTransaction(Transaction transaction) {
        new DeleteTransaction(caller).execute(TRANSACTIONS + "/" + transaction.getId());
    }

    void editTransaction(Transaction transaction) {
        int typeId = 0;
        if (transaction.getType() == ETransactionType.REGULARPAYMENT) typeId = 1;
        if (transaction.getType() == ETransactionType.REGULARINCOME) typeId = 2;
        if (transaction.getType() == ETransactionType.PURCHASE) typeId = 3;
        if (transaction.getType() == ETransactionType.INDIVIDUALINCOME) typeId = 4;
        if (transaction.getType() == ETransactionType.INDIVIDUALPAYMENT) typeId = 5;
        new EditTransaction(caller).execute(TRANSACTIONS + "/" + transaction.getId(),
                transaction.getTitle(),
                String.valueOf(transaction.getAmount()),
                LocalDateTime.of(transaction.getDate(), LocalTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")),
                LocalDateTime.of(transaction.getEndDate(), LocalTime.now()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")),
                transaction.getItemDescription(),
                String.valueOf(transaction.getTransactionInterval()),
                String.valueOf(typeId));
    }

    void editAccount(Account account) {
        String budget = String.valueOf(account.getBudget());
        String monthLimit = String.valueOf(account.getMonthLimit());
        String totalLimit = String.valueOf(account.getTotalLimit());
        new EditAccount(caller).execute(ROOT + "/account/" + context.getString(R.string.api_id), budget, monthLimit, totalLimit);
    }

    public interface TransactionsApi {
        void onTransactionsFetched(ArrayList<Transaction> transactions);

        void onTransactionsFiltered(ArrayList<Transaction> transactions);

        void onAccountFetched(Account account);

        void onTransactionAdded();

        void onAccountEdited();
    }
}
