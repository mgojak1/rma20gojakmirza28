package ba.unsa.etf.rma.rma28st;

public interface IBudgetInteractor {
    Account getAccount();

    void updateAccount(double totalLimit, double monthLimit);
}
