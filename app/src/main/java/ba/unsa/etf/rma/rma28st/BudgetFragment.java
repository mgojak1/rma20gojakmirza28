package ba.unsa.etf.rma.rma28st;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BudgetFragment extends Fragment {
    private TextView budgetView;
    private EditText totalLimitView;
    private EditText monthLimitView;
    private Button saveChanges;
    private BudgetPresenter budgetPresenter;

    private BudgetPresenter getPresenter() {
        if (budgetPresenter == null) {
            budgetPresenter = new BudgetPresenter();
        }
        return budgetPresenter;
    }

    public interface UpdateAccount {
        void updateAccount(Account account);
    }

    private UpdateAccount updateAccount;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.budget_fragment, container, false);
        budgetView = view.findViewById(R.id.budget_view);
        budgetView.setText(String.format(getString(R.string.budget), getPresenter().getAccount().getBudget()));
        totalLimitView = view.findViewById(R.id.budget_total_limit);
        totalLimitView.setText(String.valueOf(getPresenter().getAccount().getTotalLimit()));
        monthLimitView = view.findViewById(R.id.budget_month_limit);
        monthLimitView.setText(String.valueOf(getPresenter().getAccount().getMonthLimit()));
        saveChanges = view.findViewById(R.id.budget_save_changes);

        updateAccount = (UpdateAccount) getActivity();

        totalLimitView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    totalLimitView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    totalLimitView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        monthLimitView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    monthLimitView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    monthLimitView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate())
                    getPresenter().updateAccount(Double.parseDouble(totalLimitView.getText().toString()), Double.parseDouble(monthLimitView.getText().toString()));
                monthLimitView.setText(String.valueOf(getPresenter().getAccount().getMonthLimit()));
                totalLimitView.setText(String.valueOf(getPresenter().getAccount().getTotalLimit()));
                updateAccount.updateAccount(getPresenter().getAccount());
            }
        });
        return view;
    }

    private boolean validate() {
        boolean valid = true;
        if (totalLimitView.getText().toString().isEmpty()) valid = false;
        if (monthLimitView.getText().toString().isEmpty()) valid = false;
        return valid;
    }

}
