package ba.unsa.etf.rma.rma28st;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    private int resource;

    TransactionListAdapter(@NonNull Context context, int resource, ArrayList<Transaction> transactions) {
        super(context, resource, transactions);
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ConstraintLayout constraintLayout;
        if (convertView == null) {
            constraintLayout = new ConstraintLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(inflater);
            if (layoutInflater != null) {
                layoutInflater.inflate(resource, constraintLayout, true);
            }
        } else {
            constraintLayout = (ConstraintLayout) convertView;
        }

        TextView titleView = constraintLayout.findViewById(R.id.transaction_item_title);
        TextView amountView = constraintLayout.findViewById(R.id.transaction_item_amount);
        ImageView iconView = constraintLayout.findViewById(R.id.transaction_item_icon);

        Transaction transaction = getItem(position);
        if (transaction != null) {
            titleView.setText(transaction.getTitle());
            amountView.setText(String.format(Locale.getDefault(), "%.2f", transaction.getAmount()));
            if (transaction.getType().equals(ETransactionType.INDIVIDUALINCOME)) {
                iconView.setImageResource(R.mipmap.individual_income);
            } else if (transaction.getType().equals(ETransactionType.REGULARINCOME)) {
                iconView.setImageResource(R.mipmap.regular_income);
            } else if (transaction.getType().equals(ETransactionType.PURCHASE)) {
                iconView.setImageResource(R.mipmap.purchase);
            } else if (transaction.getType().equals(ETransactionType.INDIVIDUALPAYMENT)) {
                iconView.setImageResource(R.mipmap.individual_payment);
            } else if (transaction.getType().equals(ETransactionType.REGULARPAYMENT)) {
                iconView.setImageResource(R.mipmap.regular_payment);
            }
        }
        return constraintLayout;
    }

    void setTransactions(ArrayList<Transaction> transactions) {
        this.clear();
        this.addAll(transactions);
    }

    Transaction getTransaction(int position) {
        return this.getItem(position);
    }

    void sortTransactions(ArrayList<Transaction> transactions, final String sortBy, String filterBy, LocalDate localDate) {
        final ArrayList<String> sortTypes = new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.sort_by)));
        Comparator<Transaction> comparator = new Comparator<Transaction>() {
            @Override
            public int compare(Transaction t1, Transaction t2) {
                if (sortBy.equals(sortTypes.get(0))) {
                    return t1.getAmount() > t2.getAmount() ? 1 : -1;
                } else if (sortBy.equals(sortTypes.get(1))) {
                    return t1.getAmount() < t2.getAmount() ? 1 : -1;
                } else if (sortBy.equals(sortTypes.get(2))) {
                    return t1.getTitle().compareTo(t2.getTitle());
                } else if (sortBy.equals(sortTypes.get(3))) {
                    return t2.getTitle().compareTo(t1.getTitle());
                } else if (sortBy.equals(sortTypes.get(4))) {
                    return t1.getDate().compareTo(t2.getDate());
                } else if (sortBy.equals(sortTypes.get(5))) {
                    return t2.getDate().compareTo(t1.getDate());
                }
                return 0;
            }
        };
        transactions.sort(comparator);
        filterTransactions(transactions, filterBy, localDate);
    }

    void filterTransactions(ArrayList<Transaction> transactions, String filterBy, LocalDate localDate) {
        final ArrayList<String> filterTypes = new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.filter_by)));
        ETransactionType filter;
        if (filterBy.equals(filterTypes.get(1))) {
            filter = ETransactionType.INDIVIDUALPAYMENT;
        } else if (filterBy.equals(filterTypes.get(2))) {
            filter = ETransactionType.REGULARPAYMENT;
        } else if (filterBy.equals(filterTypes.get(3))) {
            filter = ETransactionType.PURCHASE;
        } else if (filterBy.equals(filterTypes.get(4))) {
            filter = ETransactionType.INDIVIDUALINCOME;
        } else if (filterBy.equals(filterTypes.get(5))) {
            filter = ETransactionType.REGULARINCOME;
        } else filter = null;
        ArrayList<Transaction> filteredTransactions = new ArrayList<>();
        for (Transaction t : transactions) {
            if (filter == null || t.getType().equals(filter)) {
                if (t.getType().equals(ETransactionType.REGULARINCOME) || t.getType().equals(ETransactionType.REGULARPAYMENT)) {
                    if ((t.getDate().isEqual(localDate) || t.getDate().isBefore(localDate)) && (t.getEndDate().isAfter(localDate) || t.getEndDate().isEqual(localDate)))
                        filteredTransactions.add(t);
                } else if (t.getDate().getYear() == localDate.getYear() && t.getDate().getMonthValue() == localDate.getMonthValue())
                    filteredTransactions.add(t);
            }
        }

        this.clear();
        this.addAll(filteredTransactions);
    }
}