package ba.unsa.etf.rma.rma28st;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetAccount extends AsyncTask<String, Void, Account> {
    private RestApi.TransactionsApi caller;

    GetAccount(RestApi.TransactionsApi caller) {
        this.caller = caller;
    }

    @Override
    protected Account doInBackground(String... strings) {
        Account account = new Account();
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                String result = stringBuilder.toString();
                JSONObject response = new JSONObject(result);
                account.setMonthLimit(response.getDouble("monthLimit"));
                account.setTotalLimit(response.getDouble("totalLimit"));
                account.setBudget(response.getDouble("budget"));
                return account;
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            return null;
        }
    }


    protected void onPostExecute(Account account) {
        caller.onAccountFetched(account);
    }
}
