package ba.unsa.etf.rma.rma28st;

public class BudgetInteractor implements IBudgetInteractor {
    @Override
    public Account getAccount() {
        return AccountModel.account;
    }

    @Override
    public void updateAccount(double totalLimit, double monthLimit) {
        getAccount().setTotalLimit(totalLimit);
        getAccount().setMonthLimit(monthLimit);
    }
}
