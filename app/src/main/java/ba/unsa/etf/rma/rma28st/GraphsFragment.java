package ba.unsa.etf.rma.rma28st;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class GraphsFragment extends Fragment {
    BarChart chart1;
    BarChart chart2;
    BarChart chart3;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.graphs_fragment, container, false);
        chart1 = view.findViewById(R.id.chart1);
        chart2 = view.findViewById(R.id.chart2);
        chart3 = view.findViewById(R.id.chart3);
        BarData paymentBarData = new BarData();
        BarData incomeBarData = new BarData();
        BarData totalBarData = new BarData();
        ArrayList<BarEntry> paymentData = new ArrayList<>();
        ArrayList<BarEntry> incomeData = new ArrayList<>();
        ArrayList<BarEntry> totalData = new ArrayList<>();

        ArrayList<Transaction> transactions = new ArrayList<>();

        for (int i = 0; i < transactions.size(); i++) {
            if (transactions.get(i).getType().equals(ETransactionType.INDIVIDUALINCOME) || transactions.get(i).getType().equals(ETransactionType.REGULARINCOME)) {
                incomeData.add(new BarEntry(i, (float) transactions.get(i).getAmount()));
            } else {
                paymentData.add(new BarEntry(i, (float) transactions.get(i).getAmount()));
            }
            totalData.add(new BarEntry(i, (float) transactions.get(i).getAmount()));
        }

        BarDataSet paymentDataSet = new BarDataSet(paymentData, "Payment");
        BarDataSet incomeDataSet = new BarDataSet(incomeData, "Income");
        BarDataSet totalDataSet = new BarDataSet(totalData, "Total");

        paymentDataSet.setColor(getContext().getColor(R.color.validRed));
        incomeDataSet.setColor(getContext().getColor(R.color.validGreen));
        totalDataSet.setColor(getContext().getColor(R.color.colorAccent));

        paymentDataSet.setDrawIcons(false);
        incomeDataSet.setDrawIcons(false);
        totalDataSet.setDrawIcons(false);

        paymentBarData.addDataSet(paymentDataSet);
        incomeBarData.addDataSet(incomeDataSet);
        totalBarData.addDataSet(totalDataSet);

        chart1.setData(paymentBarData);
        chart1.setPinchZoom(false);
        chart1.setDoubleTapToZoomEnabled(false);
        chart1.setDescription(new Description());

        chart2.setData(incomeBarData);
        chart2.setPinchZoom(false);
        chart2.setDoubleTapToZoomEnabled(false);

        chart3.setData(totalBarData);
        chart3.setPinchZoom(false);
        chart3.setDoubleTapToZoomEnabled(false);
        return view;
    }
}
