package ba.unsa.etf.rma.rma28st;

import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.net.URL;

public class DeleteTransaction extends AsyncTask<String, Void, Void> {
    private RestApi.TransactionsApi caller;

    DeleteTransaction(RestApi.TransactionsApi caller) {
        this.caller = caller;
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("DELETE");
            System.out.println("RESPONSE: " + conn.getResponseMessage());
            try {
                return null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.toString());
            return null;
        }
    }


    @Override
    protected void onPostExecute(Void v) {
        caller.onTransactionAdded();
    }
}
