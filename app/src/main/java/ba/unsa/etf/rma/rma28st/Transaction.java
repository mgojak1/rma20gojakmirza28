package ba.unsa.etf.rma.rma28st;

import android.os.Parcel;
import android.os.Parcelable;

import java.time.LocalDate;

public class Transaction implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };
    private LocalDate date;
    private double amount;
    private String title;
    private ETransactionType type;
    private String itemDescription;
    private int transactionInterval;
    private LocalDate endDate;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Transaction(LocalDate date, double amount, String title, ETransactionType type, String itemDescription, int transactionInterval, LocalDate endDate) {
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
    }

    public Transaction() {

    }

    public Transaction(Parcel in) {
        date = LocalDate.parse(in.readString());
        amount = in.readDouble();
        title = in.readString();
        type = ETransactionType.valueOf(in.readString());
        itemDescription = in.readString();
        transactionInterval = in.readInt();
        endDate = LocalDate.parse(in.readString());
        id = in.readInt();
    }

    LocalDate getDate() {
        return date;
    }

    void setDate(LocalDate date) {
        this.date = date;
    }

    double getAmount() {
        return amount;
    }

    void setAmount(double amount) {
        this.amount = amount;
    }

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    ETransactionType getType() {
        return type;
    }

    void setType(ETransactionType type) {
        this.type = type;
    }

    String getItemDescription() {
        return itemDescription;
    }

    void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    int getTransactionInterval() {
        return transactionInterval;
    }

    void setTransactionInterval(int transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    LocalDate getEndDate() {
        return endDate;
    }

    void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date.toString());
        dest.writeDouble(amount);
        dest.writeString(title);
        dest.writeString(type.toString());
        dest.writeString(itemDescription);
        dest.writeInt(transactionInterval);
        dest.writeString(endDate.toString());
        dest.writeInt(id);
    }
}
