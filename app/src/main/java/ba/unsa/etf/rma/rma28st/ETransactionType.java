package ba.unsa.etf.rma.rma28st;

public enum ETransactionType {
    INDIVIDUALPAYMENT, REGULARPAYMENT, PURCHASE, INDIVIDUALINCOME, REGULARINCOME
}
