package ba.unsa.etf.rma.rma28st;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class FilterTransactions extends AsyncTask<String, Void, ArrayList<Transaction>> {

    private RestApi.TransactionsApi caller;

    FilterTransactions(RestApi.TransactionsApi caller) {
        this.caller = caller;
    }

    @Override
    protected ArrayList<Transaction> doInBackground(String... strings) {
        // TODO: Parse enum and interval, pages
        ArrayList<Transaction> transactions = new ArrayList<>();
        try {
            int page = 0;
            do {
                URL url = new URL(strings[0] + "/filter" + "?page=" + page + "&" + strings[1]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    String result = stringBuilder.toString();
                    JSONObject response = new JSONObject(result);
                    JSONArray jsonTransactions = response.getJSONArray("transactions");
                    for (int i = 0; i < jsonTransactions.length(); i++) {
                        Transaction transaction = new Transaction();
                        JSONObject object = jsonTransactions.getJSONObject(i);
                        transaction.setTitle(object.getString("title"));
                        if (object.getInt("TransactionTypeId") == 1) {
                            transaction.setType(ETransactionType.REGULARPAYMENT);
                        } else if (object.getInt("TransactionTypeId") == 2) {
                            transaction.setType(ETransactionType.REGULARINCOME);
                        } else if (object.getInt("TransactionTypeId") == 3) {
                            transaction.setType(ETransactionType.PURCHASE);
                        } else if (object.getInt("TransactionTypeId") == 4) {
                            transaction.setType(ETransactionType.INDIVIDUALINCOME);
                        } else {
                            transaction.setType(ETransactionType.INDIVIDUALPAYMENT);
                        }
                        try {
                            transaction.setTransactionInterval(object.getInt("transactionInterval"));
                        } catch (Exception e) {
                            transaction.setTransactionInterval(0);
                        }
                        try {
                            transaction.setId(object.getInt("id"));
                        } catch (Exception e) {
                            transaction.setId(0);
                        }
                        transaction.setAmount(object.getDouble("amount"));
                        transaction.setItemDescription(object.getString("itemDescription"));
                        transaction.setAmount(object.getDouble("amount"));
                        transaction.setDate(LocalDate.parse(object.getString("date"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")));
                        try {
                            transaction.setEndDate(LocalDate.parse(object.getString("endDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")));
                        } catch (Exception e) {
                            transaction.setEndDate(LocalDate.parse(object.getString("date"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")));
                        }
                        transactions.add(transaction);
                    }
                    if (jsonTransactions.length() < 5) return transactions;
                    page++;
                } finally {
                    urlConnection.disconnect();
                }
            } while (true);
        } catch (Exception e) {
            return null;
        }
    }


    protected void onPostExecute(ArrayList<Transaction> transactions) {
        caller.onTransactionsFiltered(transactions);
    }
}
