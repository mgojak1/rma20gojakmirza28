package ba.unsa.etf.rma.rma28st;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

@SuppressWarnings("ConstantConditions")
public class TransactionDetailFragment extends Fragment implements TransactionDetailView {
    private TextView titleView;
    private TextView amountView;
    private TextView dateView;
    private TextView endDateView;
    private TextView descriptionView;
    private TextView intervalView;
    private Spinner typeSpinner;
    private TransactionManager transactionManager;
    private LocalDate date;
    private LocalDate endDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transaction_detail_fragment, container, false);
        titleView = view.findViewById(R.id.transaction_title);
        amountView = view.findViewById(R.id.transaction_amount);
        dateView = view.findViewById(R.id.transaction_date);
        endDateView = view.findViewById(R.id.transaction_end_date);
        descriptionView = view.findViewById(R.id.transaction_description);
        intervalView = view.findViewById(R.id.transaction_interval);
        typeSpinner = view.findViewById(R.id.transaction_type);
        Button saveButton = view.findViewById(R.id.save_button);
        Button deleteButton = view.findViewById(R.id.delete_button);
        Button dateNextButton = view.findViewById(R.id.next_date_button);
        Button datePreviousButton = view.findViewById(R.id.prevous_date_button);
        Button endDateNextButton = view.findViewById(R.id.next_end_date_button);
        Button endDatePreviousButton = view.findViewById(R.id.previous_end_date_button);

        transactionManager = (TransactionManager) getActivity();

        typeSpinner.setAdapter(new FilterSpinnerAdapter(getActivity(), R.layout.filter_spinner_item,
                new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.transaction_types)))));
        date = LocalDate.now();
        endDate = LocalDate.now().plusDays(1);

        Transaction transaction = null;
        if (getArguments() != null && getArguments().containsKey("transaction"))
            transaction = getArguments().getParcelable("transaction");

        deleteButton.setEnabled(transaction != null);

        if (transaction != null) {
            titleView.setText(transaction.getTitle());
            amountView.setText(String.format(Locale.getDefault(), "%.2f", transaction.getAmount()));
            date = transaction.getDate();
            endDate = transaction.getEndDate();
            descriptionView.setText(transaction.getItemDescription());
            intervalView.setText(String.valueOf(transaction.getTransactionInterval()));
            if (transaction.getType().equals(ETransactionType.REGULARPAYMENT))
                typeSpinner.setSelection(0);
            if (transaction.getType().equals(ETransactionType.REGULARINCOME))
                typeSpinner.setSelection(1);
            if (transaction.getType().equals(ETransactionType.PURCHASE))
                typeSpinner.setSelection(2);
            if (transaction.getType().equals(ETransactionType.INDIVIDUALINCOME))
                typeSpinner.setSelection(3);
            if (transaction.getType().equals(ETransactionType.INDIVIDUALPAYMENT))
                typeSpinner.setSelection(4);
        }
        updateDate(0);
        updateEndDate(0);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) return;
                Transaction transaction = null;
                if (getArguments() != null && getArguments().containsKey("transaction"))
                    transaction = getArguments().getParcelable("transaction");
                boolean newT = false;
                if (transaction == null) {
                    newT = true;
                    transaction = new Transaction();
                }

                transaction.setTitle(titleView.getText().toString());
                transaction.setItemDescription(descriptionView.getText().toString());
                transaction.setAmount(Double.parseDouble(amountView.getText().toString().replace(",", ".")));
                transaction.setTransactionInterval(Integer.parseInt(intervalView.getText().toString()));
                transaction.setDate(date);
                transaction.setEndDate(endDate);
                int type = typeSpinner.getSelectedItemPosition();
                if (type == 0) transaction.setType(ETransactionType.REGULARPAYMENT);
                else if (type == 1) transaction.setType(ETransactionType.REGULARINCOME);
                else if (type == 2) transaction.setType(ETransactionType.PURCHASE);
                else if (type == 3) transaction.setType(ETransactionType.INDIVIDUALINCOME);
                else if (type == 4) transaction.setType(ETransactionType.INDIVIDUALPAYMENT);

                if (!validateBudget(transaction)) {
                    final Transaction finalTransaction = transaction;
                    final boolean finalNewT = newT;
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Insufficient budget or limit")
                            .setMessage("Budget too small or amount over limit, continue?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (finalNewT) {
                                        transactionManager.addTransaction(finalTransaction);
                                    } else transactionManager.transactionChanged(finalTransaction);
                                }
                            })
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                    if (newT) {
                        transactionManager.addTransaction(transaction);
                    }
                    transactionManager.transactionChanged(transaction);
                }
            }
        });

        dateNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate(1);
            }
        });

        datePreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate(-1);
            }
        });

        dateNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate(1);
            }
        });

        endDateNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEndDate(1);
            }
        });

        endDatePreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEndDate(-1);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(getActivity())
                        .setTitle("Delete transaction")
                        .setMessage("Are you sure you want to delete this transaction?")
                        .setPositiveButton(R.string.confirm_delete, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                transactionManager.deleteTransaction(getArguments().getInt("position", -1));
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        titleView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    titleView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    titleView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        amountView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    amountView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    amountView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        descriptionView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    descriptionView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    descriptionView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        intervalView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    intervalView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    intervalView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dateView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    dateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    dateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        endDateView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    endDateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
                } else
                    endDateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return view;
    }

    @Override
    public boolean validate() {
        boolean valid = true;
        if (titleView.getText().toString().isEmpty()) {
            valid = false;
            titleView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
        }
        if (intervalView.getText().toString().isEmpty()) {
            valid = false;
            intervalView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
        }
        if (dateView.getText().toString().isEmpty()) {
            valid = false;
            dateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
        }
        if (endDateView.getText().toString().isEmpty()) {
            valid = false;
            endDateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
        }
        if (descriptionView.getText().toString().isEmpty()) {
            valid = false;
            descriptionView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
        }
        if (amountView.getText().toString().isEmpty()) {
            valid = false;
            amountView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
        }
        if (endDate.isBefore(date)) {
            valid = false;
            endDateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validRed)));
        }

        if (valid) {
            titleView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            amountView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            dateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            intervalView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            endDateView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
            descriptionView.setBackgroundTintList(ColorStateList.valueOf(getContext().getColor(R.color.validGreen)));
        }
        return valid;
    }

    @Override
    public void updateDate(int monthChange) {
        if (monthChange == -1)
            date = date.minusMonths(1);
        else if (monthChange == 1) date = date.plusMonths(1);
        String month = date.getMonth().toString().substring(0, 1).toUpperCase().concat(date.getMonth().toString().substring(1).toLowerCase());
        dateView.setText(String.format(Locale.getDefault(), "%s, %d", month, date.getYear()));
    }

    @Override
    public void updateEndDate(int monthChange) {
        if (monthChange == -1)
            endDate = endDate.minusMonths(1);
        else if (monthChange == 1) endDate = endDate.plusMonths(1);
        String month = endDate.getMonth().toString().substring(0, 1).toUpperCase().concat(endDate.getMonth().toString().substring(1).toLowerCase());
        endDateView.setText(String.format(Locale.getDefault(), "%s, %d", month, endDate.getYear()));
    }

    @Override
    public boolean validateBudget(Transaction transaction) {
        boolean valid = true;
        BudgetPresenter presenter = new BudgetPresenter();
        Account account = presenter.getAccount();
        if (account != null && (transaction.getType().equals(ETransactionType.REGULARPAYMENT) ||
                transaction.getType().equals(ETransactionType.PURCHASE) ||
                transaction.getType().equals(ETransactionType.INDIVIDUALPAYMENT)) &&
                (transaction.getAmount() > account.getBudget() ||
                        transaction.getAmount() > account.getTotalLimit())) {
            valid = false;
        }
        return valid;
    }

    public interface TransactionManager {
        void addTransaction(Transaction transaction);

        void deleteTransaction(int position);

        void transactionChanged(Transaction transaction);
    }
}
