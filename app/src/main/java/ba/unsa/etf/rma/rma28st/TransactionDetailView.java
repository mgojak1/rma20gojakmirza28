package ba.unsa.etf.rma.rma28st;

public interface TransactionDetailView {
    boolean validate();

    boolean validateBudget(Transaction transaction);

    void updateEndDate(int monthChange);

    void updateDate(int monthChange);
}
