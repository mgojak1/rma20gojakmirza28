package ba.unsa.etf.rma.rma28st;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FragmentViewPagerAdapter extends FragmentStatePagerAdapter {
    public FragmentViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position % 3 == 1) {
            return new BudgetFragment();
        } else if (position % 3 == 2) return new RootFragment();
        else return new GraphsFragment();
    }

    @Override
    public int getCount() {
        return 300;
    }

}
