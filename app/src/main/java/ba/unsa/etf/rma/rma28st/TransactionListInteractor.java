package ba.unsa.etf.rma.rma28st;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;

import java.time.LocalDate;
import java.util.ArrayList;

public class TransactionListInteractor implements ITransactionListInteractor {
    private Context context;
    TransactionListInteractor(Context context) {
        this.context = context;
    }
    @Override
    public ArrayList<Transaction> getTransactions() {
        ArrayList<Transaction> transactions = new ArrayList<>();
        String URL = "content://ba.unsa.etf.rma.rma28st.cp/transactions";
        Uri tr = Uri.parse(URL);
        Cursor c = context.getContentResolver().query(tr, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                Transaction t = new Transaction();
                t.setId(c.getInt(c.getColumnIndex(DatabaseHelper.TRANSACTION_INTERNAL_ID)));
                t.setTransactionInterval(c.getInt(c.getColumnIndex(DatabaseHelper.TRANSACTION_INTERVAL)));
                t.setType(ETransactionType.valueOf(c.getString(c.getColumnIndex(DatabaseHelper.TRANSACTION_TYPE))));
                t.setDate(LocalDate.parse(c.getString(c.getColumnIndex(DatabaseHelper.TRANSACTION_DATE))));
                t.setEndDate(LocalDate.parse(c.getString(c.getColumnIndex(DatabaseHelper.TRANSACTION_END_DATE))));
                t.setAmount(c.getDouble(c.getColumnIndex(DatabaseHelper.TRANSACTION_AMOUNT)));
                t.setItemDescription(c.getString(c.getColumnIndex(DatabaseHelper.TRANSACTION_DESCRIPTION)));
                t.setTitle(c.getString(c.getColumnIndex(DatabaseHelper.TRANSACTION_TITLE)));
                transactions.add(t);
            } while (c.moveToNext());
        }
        return transactions;
    }

    @Override
    public Account getAccount() {
        return AccountModel.account;
    }

    @Override
    public void addTransaction(Transaction transaction) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TRANSACTION_INTERVAL, transaction.getTransactionInterval());
        values.put(DatabaseHelper.TRANSACTION_TYPE, String.valueOf(transaction.getType()));
        values.put(DatabaseHelper.TRANSACTION_DESCRIPTION, transaction.getItemDescription());
        values.put(DatabaseHelper.TRANSACTION_DATE, String.valueOf(transaction.getDate()));
        values.put(DatabaseHelper.TRANSACTION_END_DATE, String.valueOf(transaction.getEndDate()));
        values.put(DatabaseHelper.TRANSACTION_TITLE, "Offline transaction " + transaction.getTitle());
        values.put(DatabaseHelper.TRANSACTION_AMOUNT, transaction.getAmount());
        Uri uri = context.getContentResolver().insert(TransactionsProvider.CONTENT_URI, values);
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
        int uri = context.getContentResolver().delete(Uri.parse(TransactionsProvider.CONTENT_URI + "/" + transaction.getId()), null, null);
    }

    @Override
    public void editTransaction(Transaction transaction) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.TRANSACTION_INTERVAL, transaction.getTransactionInterval());
        values.put(DatabaseHelper.TRANSACTION_TYPE, String.valueOf(transaction.getType()));
        values.put(DatabaseHelper.TRANSACTION_DESCRIPTION, transaction.getItemDescription());
        values.put(DatabaseHelper.TRANSACTION_DATE, String.valueOf(transaction.getDate()));
        values.put(DatabaseHelper.TRANSACTION_END_DATE, String.valueOf(transaction.getEndDate()));
        values.put(DatabaseHelper.TRANSACTION_TITLE, transaction.getTitle());
        values.put(DatabaseHelper.TRANSACTION_AMOUNT, transaction.getAmount());
        int uri = context.getContentResolver().update(Uri.parse(TransactionsProvider.CONTENT_URI + "/" + transaction.getId()), values, null, null);
    }

    @Override
    public void editAccount(Account account) {
        AccountModel.account.setBudget(account.getBudget());
        AccountModel.account.setTotalLimit(account.getTotalLimit());
        AccountModel.account.setMonthLimit(account.getMonthLimit());
    }
}
