package ba.unsa.etf.rma.rma28st;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.Arrays;

public class FilterSpinnerAdapter extends ArrayAdapter<String> {
    private int resource;
    private TextView filterType;
    private ImageView filterIcon;

    FilterSpinnerAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> items) {
        super(context, resource, items);
        this.resource = resource;
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ConstraintLayout constraintLayout;
        if (convertView == null) {
            constraintLayout = new ConstraintLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(inflater);
            if (layoutInflater != null) {
                layoutInflater.inflate(resource, constraintLayout, true);
            }
        } else {
            constraintLayout = (ConstraintLayout) convertView;
        }

        filterType = constraintLayout.findViewById(R.id.filter_spinner_type);
        filterIcon = constraintLayout.findViewById(R.id.filter_spinner_icon);

        filterType.setText(getItem(position));
        ArrayList<String> filters = new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.transaction_types)));
        String item = getItem(position);
        if (item == null) item = "";
        if (item.equals("All Transactions")) {
            filterIcon.setImageResource(R.mipmap.all_transactions);
        } else if (item.equals(filters.get(0))) {
            filterIcon.setImageResource(R.mipmap.regular_payment);
        } else if (item.equals(filters.get(1))) {
            filterIcon.setImageResource(R.mipmap.regular_income);
        } else if (item.equals(filters.get(2))) {
            filterIcon.setImageResource(R.mipmap.purchase);
        } else if (item.equals(filters.get(3))) {
            filterIcon.setImageResource(R.mipmap.individual_income);
        } else if (item.equals(filters.get(4))) {
            filterIcon.setImageResource(R.mipmap.individual_payment);
        }
        return constraintLayout;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ConstraintLayout constraintLayout;
        if (convertView == null) {
            constraintLayout = new ConstraintLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(inflater);
            if (layoutInflater != null) {
                layoutInflater.inflate(resource, constraintLayout, true);
            }
        } else {
            constraintLayout = (ConstraintLayout) convertView;
        }

        filterType = constraintLayout.findViewById(R.id.filter_spinner_type);
        filterIcon = constraintLayout.findViewById(R.id.filter_spinner_icon);

        filterType.setText(getItem(position));
        ArrayList<String> filters = new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.transaction_types)));
        String item = getItem(position);
        if (item == null) item = "";
        if (item.equals("All Transactions")) {
            filterIcon.setImageResource(R.mipmap.all_transactions);
        } else if (item.equals(filters.get(0))) {
            filterIcon.setImageResource(R.mipmap.regular_payment);
        } else if (item.equals(filters.get(1))) {
            filterIcon.setImageResource(R.mipmap.regular_income);
        } else if (item.equals(filters.get(2))) {
            filterIcon.setImageResource(R.mipmap.purchase);
        } else if (item.equals(filters.get(3))) {
            filterIcon.setImageResource(R.mipmap.individual_income);
        } else if (item.equals(filters.get(4))) {
            filterIcon.setImageResource(R.mipmap.individual_payment);
        }
        return constraintLayout;
    }
}
